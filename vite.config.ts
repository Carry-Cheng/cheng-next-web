import type { UserConfig, ConfigEnv } from 'vite'
import { resolve } from 'path'
import { loadEnv } from 'vite'
import { wrapperEnv } from './build/utils'
import { createAlias } from './build/vite/alias'
import { CreateVitePlugins } from './build/vite/plugins/index'

export default ({ command, mode }: ConfigEnv): UserConfig => {

  const root = process.cwd()

  const env = loadEnv(mode, root)

  // The boolean type read by loadEnv is a string. This function can be converted to boolean type
  const viteEnv = wrapperEnv(env)

  const { VITE_PORT, VITE_PUBLIC_BASE, VITE_BUILD_OUT_DIR, VITE_DROP_CONSOLE } = viteEnv

  const isBuild = command === 'build'

  return {
    base: VITE_PUBLIC_BASE,
    root,
    resolve: {
      alias: createAlias([
        // @/xxxx => src/xxxx
        ['@', 'src'],
        // #/xxxx => types/xxxx
        ['#', 'types']
      ])
    },
    server: {
      port: VITE_PORT,
      fs: {
        strict: true,
        allow: ['..', '../@', '../#']
      }
    },
    build: {
      outDir: VITE_BUILD_OUT_DIR,
      terserOptions: {
        compress: {
          keep_infinity: true,
          // Used to delete console in production environment
          drop_console: VITE_DROP_CONSOLE,
          drop_debugger: true
        },
      },
      // Turning off brotliSize display can slightly reduce packaging time
      brotliSize: false,
      chunkSizeWarningLimit: 1200,
    },

    css: {
      preprocessorOptions: {
        less: {
          modifyVars: {
            hack: `true; @import (reference) "${resolve(
              'src/design/global-variable.less'
            )}";`
          },
          javascriptEnabled: true,
        }
      }
    },

    // The vite plugin used by the project. The quantity is large, so it is separately extracted and managed
    plugins: CreateVitePlugins(viteEnv, isBuild)
  }
}

