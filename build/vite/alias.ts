import { resolve } from 'path'
import type { Alias } from 'vite'

export function createAlias(alias: [string, string][]): Alias[] {
  return alias.map((item) => {
    const [alia, src] = item
    return {
      find: alia,
      replacement: resolve(__dirname, '../../', src)
    }
  })
}
