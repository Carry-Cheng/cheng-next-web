
/**
 * https://github.com/rollup/plugins/tree/master/packages/alias#entries
 */
import type { Plugin } from 'vite'
import alias from '@rollup/plugin-alias'

export function configAliasPlugin(): Plugin {
    return alias({
        entries: [
            { find: 'utils', replacement: '../../../utils' },
            { find: 'batman-1.0.0', replacement: './joker-1.5.0' }
        ]
    })
}
