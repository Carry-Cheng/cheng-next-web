import type { Plugin } from 'vite'

import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import legacy from '@vitejs/plugin-legacy'

import PurgeIcons from 'vite-plugin-purge-icons'

import { configStyleImportPlugin } from './style-import'
import { configHtmlPlugin } from './html'
import { configCompressPlugin } from './compress'
import { configVisualizerConfig } from './visualizer'
import { configImageminPlugin } from './imagemin'
import { configWindiCssPlugin } from './windicss'
import { configSvgIconsPlugin } from './svgSprite'

export function CreateVitePlugins(viteEnv: ViteEnv, isBuild: boolean) {
  const { VITE_USE_IMAGEMIN, VITE_BUILD_COMPRESS } = viteEnv

  const vitePlugins: (Plugin | Plugin[])[] = [
    // have to
    vue(),
    // have to
    vueJsx(),
  ]

  // @rollup/plugin-alias
  // todo

  // @vitejs/plugin-legacy
  isBuild && vitePlugins.push(
    legacy({
        targets: ['defaults', 'not IE 11'],
    })
  )

  // vite-plugin-html
  vitePlugins.push(configHtmlPlugin(viteEnv, isBuild))

  // vite-plugin-svg-icons
  vitePlugins.push(configSvgIconsPlugin(isBuild))

  // vite-plugin-windicss
  vitePlugins.push(configWindiCssPlugin())

  // vite-plugin-purge-icons
  vitePlugins.push(PurgeIcons())

  // vite-plugin-style-import
  vitePlugins.push(configStyleImportPlugin())

  // rollup-plugin-visualizer
  vitePlugins.push(configVisualizerConfig())

  // The following plugins only work in the production environment
  if (isBuild) {
    //vite-plugin-imagemin
    VITE_USE_IMAGEMIN && vitePlugins.push(configImageminPlugin())

    // rollup-plugin-gzip
    vitePlugins.push(configCompressPlugin(VITE_BUILD_COMPRESS))
  }

  return vitePlugins
}
