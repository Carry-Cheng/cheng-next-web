/**
 * Package file volume analysis
 */
import type { Plugin } from 'vite'
import visualizer from 'rollup-plugin-visualizer'
import { isReportMode } from '../../utils'

export function configVisualizerConfig(): Plugin | Plugin[] {
  if (isReportMode()) {
    return visualizer({
      filename: './node_modules/.cache/visualizer/stats.html',
      open: true,
      // @ts-ignore
      gzipSize: true,
      // @ts-ignore
      brotliSize: true,
    }) as Plugin
  }
  return []
}
