/**
 * Plugin to minimize and use ejs template syntax in index.html.
 * https://github.com/anncwb/vite-plugin-html
 */
import type { Plugin } from 'vite'

import html from 'vite-plugin-html'

import pkg from '../../../package.json'

export function configHtmlPlugin(env: ViteEnv, isBuild: boolean): Plugin[] {
  const GLOB_CONFIG_FILE_NAME = '_app.config.js'
  const { VITE_GLOB_APP_TITLE, VITE_PUBLIC_BASE } = env

  const path = VITE_PUBLIC_BASE.endsWith('/') ? VITE_PUBLIC_BASE : `${VITE_PUBLIC_BASE}/`

  const getAppConfigSrc = () => {
    return `${path || '/'}${GLOB_CONFIG_FILE_NAME}?v=${pkg.version}-${new Date().getTime()}`
  }

  return html({
    minify: isBuild,
    inject: {
      // Inject data into ejs template
      injectData: {
        title: VITE_GLOB_APP_TITLE,
      },
      // Embed the generated app.config.js file
      tags: isBuild
        ? [
            {
              tag: 'script',
              attrs: {
                src: getAppConfigSrc(),
              },
            },
          ]
        : [],
    },
  })
}
