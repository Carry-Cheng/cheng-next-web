declare interface ViteEnv {
    VITE_PORT: number
    VITE_PUBLIC_BASE: string
    VITE_BUILD_OUT_DIR: string
    VITE_DROP_CONSOLE: boolean
    VITE_BUILD_COMPRESS: 'gzip' | 'brotli' | 'none'
    VITE_LEGACY: boolean
    VITE_USE_IMAGEMIN: boolean
    VITE_GLOB_APP_TITLE: string
}

declare interface ImportMetaEnv extends ViteEnv {
    __: unknown
}
