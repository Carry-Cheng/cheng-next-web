/// <reference types="../node_modules/@types/three/index.d.ts" />

declare type Recordable<T extends any = any> = Record<string, T>

declare type ReadonlyRecordable<T extends any = any> = {
  readonly [key: string]: T
}


// privide

declare interface ProvideUserInfo {
  name: string
  age: number
}