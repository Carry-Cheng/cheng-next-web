import { App, ref, unref, computed } from 'vue'
import { launchRouter } from '@/router/index'
import { launchPlugin } from '@/plugins/index'

export default function(app: App) {
    // launch router
    launchRouter(app)

    const user = ref<ProvideUserInfo>({
        name: 'zhoucheng',
        age: 18
    })

    // 测试
    // app.provide<ProvideUserInfo>('__GLOBAL__', unref(computed(() => unref(user))))
    app.provide<ProvideUserInfo>('__GLOBAL__', unref(user))

    setTimeout(() => {
        console.info('-----------5s reset: -----------')
        user.value.name = 'zhoucheng2222222222'
    }, 5000)

    // launch plugin
    launchPlugin(app)
}
