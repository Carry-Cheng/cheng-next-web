import { Engine } from 'babylonjs'

/**
 * 创建一个3D Engine
 * @param canvas HTMLCanvasElement
 * @returns 3D Engine
 */
export const CreateEngine = (canvas: HTMLCanvasElement) => {
    return new Engine(
        // canvas or context
        canvas,
        // defines enable antialiasing (default: false)
        // 定义启用抗锯齿（默认值：false）
        true,
        // defines further options to be sent to the getContext() function
        // 定义要发送到getContext()函数的其他选项
        /**
         * @see [EngineOptions](https://doc.babylonjs.com/typedoc/interfaces/babylon.engineoptions)
         */
        {
            /**
             * Defines whether to adapt to the device's viewport characteristics (default: false)
             * 定义是否适应设备的视口特征(默认值：false)
             * @see
             * 
             */
            // adaptToDeviceRatioSearch: false,
            preserveDrawingBuffer: true,
            stencil: true
        }
    )
}