import { inject, unref } from 'vue'

export const useUserInfo = () => {
  const user = inject<ProvideUserInfo>('__GLOBAL__')
  return unref(user)
}

export default {}
