import style from '@/config/style'

/**
 * 生成样式Name
 * @param suffixCls 样式后缀
 * @returns 
 */
export const useDesign = (suffixCls: string) => {
    return `${style.prefixCls}-${suffixCls}`
}
