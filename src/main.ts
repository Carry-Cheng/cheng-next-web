import { createApp } from 'vue'
import App from '@/App.vue'
import launch from '@/launch'
import '@/design/index.less'

(async () => {
    
    const app = createApp(App)

    launch(app)

    app.mount('#root', true)

})()
