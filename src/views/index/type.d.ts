/**
 * 夜空
 * 场景包含的元素
 */
declare interface SceneElements {
    /**
     * 星星
     */
    stars: THREE.Points
}

declare class NightSky {
    
    constructor(
        canvas: HTMLCanvasElement,
        context: WebGL2RenderingContext,
        width: number,
        height: number
    ) {}

}