import * as THREE from 'three'
/**
 * 夜空
 */
export class NightSky {
    private canvas: HTMLCanvasElement
    private context: WebGL2RenderingContext
    private width: number
    private height: number
    private renderer: THREE.WebGLRenderer

    /**
     * 场景
     */
    private scene: THREE.Scene

    /**
     * 相机(透视摄像机)
     */
    private camera: THREE.PerspectiveCamera

    /**
     * 场景包含的元素
     */
    private sceneElements: SceneElements
    
    /**
     * 创建一个夜空
     * @param canvas WebGLRender需要
     * @param context WebGLRender需要
     * @param width 夜空宽度，默认值屏幕宽度
     * @param height 夜空高度，默认值屏幕高度
     */
    constructor(
        canvas: HTMLCanvasElement,
        context: WebGL2RenderingContext,
        width: number = window.innerWidth,
        height: number = window.innerHeight
    ) {
        this.canvas = canvas
        this.context = context
        this.width = width
        this.height = height
        // 初始化场景
        this.scene = new THREE.Scene()
        this.scene.background = new THREE.Color(0x242424)
        // this.scene.fog = new THREE.Fog(0x050505, 2000, 3500)
        
        // 初始化场景元素
        this.sceneElements = {
            stars: new THREE.Points()
        }
        // 初始化相机(透视相机)
        this.camera = new THREE.PerspectiveCamera(
            75, // fov: 视野角度
            this.width / this.height, // asprect: 长宽比
            5, // near: 近截面
            3500 // far: 远界面
        )
        // 默认情况下，当我们调用scene.add()的时候，物体将会被添加到(0,0,0)坐标。
        // 但将使得摄像机和立方体彼此在一起。为了防止这种情况的发生，我们只需要将摄像机稍微向外移动一些即可。
        this.camera.position.z = 200
        // 初始化渲染器
        this.renderer = new THREE.WebGLRenderer({
            canvas: this.canvas,
            context: this.context
        })
        // 阴影效果
        this.renderer.shadowMap.enabled = true
        this.renderer.shadowMap.type = THREE.PCFSoftShadowMap
        this.renderer.setSize(this.width, this.height)
    }

    /**
     * 起飞
     */
    public launcher() {
        // 创建各种元素到场景
        this.draw()
        // 渲染场景 渲染循环(render loop)或者动画循环(animate loop)
        this.animate()
    }

    /**
     * 渲染场景 渲染循环(render loop)或者动画循环(animate loop)
     */
    private animate() {
        const time = Date.now() * 0.001
        // this.sceneElements.stars.rotation.x = time * 0.01
        // this.sceneElements.stars.rotation.y = time * 0.008
        // this.sceneElements.stars.rotation.z = time * 0.01
        this.renderer.render(this.scene, this.camera)
        // 注意：这里需要绑定this
        requestAnimationFrame(this.animate.bind(this))
    }

    private draw() {
        // 
        this.draw1()
        // this.drawStar()
    }

    private draw1() {
        const particles = 1
        

        const stars = []
        
        const n = this.width * 2

        for (let index = 0; index < particles; index++) {
            const geometry = new THREE.SphereBufferGeometry(50, 32, 32)
            // positions
            const x = THREE.MathUtils.randFloatSpread(n)
            const y = THREE.MathUtils.randFloatSpread(n)
            const z = THREE.MathUtils.randFloatSpread(200) + 800
            console.info(geometry.attributes.position)
            // colors
            let color = new THREE.Color(Math.random(), Math.random(), Math.random())
            // const vx = (x / n) + 0.8
            // const vy = (y / n) + 0.8
            // const vz = (z / n) + 0.8
            // color.setRGB(vx, vy, vz)
            // colors.push(color.r, color.g, color.b)

            // geometry.setAttribute('position', new THREE.Float32BufferAttribute([x, y, z], 3))
            // geometry.setAttribute('color', new THREE.Float32BufferAttribute([color.r, color.g, color.b], 3))
            
            // geometry.computeBoundingSphere()

            // 
            const material = new THREE.MeshDistanceMaterial({
                farDistance: 80,
                // nearDistance: 100
            })
            // const material = new THREE.MeshBasicMaterial({ color: color.clone() })


            const star = new THREE.Mesh(geometry, material)

            star.position.set(10, 40, 0)

            this.scene.add(star)


            // 添加平行光
            // const directionalLight = new THREE.DirectionalLight(color, 10)
            // directionalLight.position.set(10, 40, 0)
            // this.scene.add(directionalLight)

            // 点光源
            const pointLight = new THREE.PointLight(0xff0000, 100)
            pointLight.position.set(10, 40, 0)
            this.scene.add(pointLight)
        }

        


    }

    private drawStar() {
        // 粒子
        const particles = 2000
        const geometry = new THREE.BufferGeometry()

        const positions = []
        const colors = []
        let color = new THREE.Color()
        const n = this.width * 2

        for (let index = 0; index < particles; index++) {
            // positions
            const x = THREE.MathUtils.randFloatSpread(n)
            const y = THREE.MathUtils.randFloatSpread(n)
            const z = THREE.MathUtils.randFloatSpread(200) + 800
            positions.push(x, y, z)
            // colors
            const vx = (x / n) + 0.8
            const vy = (y / n) + 0.8
            const vz = (z / n) + 0.8
            color.setRGB(vx, vy, vz)
            colors.push(color.r, color.g, color.b)
        }

        geometry.setAttribute('position', new THREE.Float32BufferAttribute(positions, 3))
        geometry.setAttribute('color', new THREE.Float32BufferAttribute(colors, 3))
        
        geometry.computeBoundingSphere()

        // 
        const material = new THREE.PointsMaterial({ vertexColors: true, size: 40 })


        this.sceneElements.stars = new THREE.Points(geometry, material)

        this.scene.add(this.sceneElements.stars)

    }



}
