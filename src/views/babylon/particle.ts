
/**
 * 粒子
 */
export class BeautifulParticle {
    private canvas: HTMLCanvasElement
    private webgl: WebGLRenderingContext | null
    
    /**
     * 创建一个夜空
     * @param canvas WebGLRender需要
     */
    constructor(
        canvas: HTMLCanvasElement
    ) {
        this.canvas = canvas
        this.webgl = this.canvas.getContext('webgl')
        this.launcher()
    }

    /**
     * 启动
     */
    public launcher() {
        if (!this.webgl) {
            return
        }

        // 创建 顶点着色器
        const vertexShader = this.webgl.createShader(this.webgl.VERTEX_SHADER)

        if (!vertexShader) {
            return
        }

        // 设置 WebGLShader 着色器（顶点着色器及片元着色器）的GLSL程序代码
        const vertexShaderSource = `
            precision highp float;
            attribute vec3 aPosition;
            uniform vec2 uResolution;
            varying float dist;
            void main() {
                dist = aPosition.z * 0.3;
                gl_PointSize = 3.0;
                gl_Position = vec4(
                    
                );
            }
        `
        this.webgl.shaderSource(vertexShader, vertexShaderSource)

        // 片元着色器 FragementShader
    }

}
