// import * as BABYLON from 'babylonjs'
import {
    Scene,
    Engine,
    ArcRotateCamera,
    Vector3,
    HemisphericLight,
    Mesh,
    Color3,
    Color4,
    StandardMaterial
} from 'babylonjs'

/**
 * 夜空
 */
export class NightSky {
    private canvas: HTMLCanvasElement
    private engine: Engine

    /**
     * 场景
     */
    private scene: Scene

    /**
     * 相机(`FreeCamera`)
     */
    private camera: ArcRotateCamera

    /**
     * 场景包含的元素
     */
    // private sceneElements: SceneElements
    
    /**
     * 创建一个夜空
     * @param canvas WebGLRender需要
     */
    constructor(
        canvas: HTMLCanvasElement
    ) {
        this.canvas = canvas
        // 创建3D Engine
        this.engine = new Engine(canvas, true, {
            preserveDrawingBuffer: true,
            stencil: true
        })

        // 创建一个基础BJS 场景对象
        this.scene = new Scene(this.engine)

        // 设置背景色
        this.scene.clearColor =  Color4.FromHexString("000000")
        this.scene.ambientColor = Color3.Green()

        // 创建一个自由摄影机 并将其位置设置为{x:0，y:5，z:-10}
        this.camera = new ArcRotateCamera(
            'camera1',
            Math.PI / 2,
            Math.PI / 2,
            Math.PI,
            new Vector3(0, 5, -10),
            this.scene
        )

        // 将摄影机定位到场景原点
        this.camera.setTarget(Vector3.Zero())

        // 将相机连接到画布上
        this.camera.attachControl(this.canvas, false)

        // 创建一个基本的灯光，瞄准天空的0，1，0
        // const light = new HemisphericLight('light1', new Vector3(0, 1, 0), this.scene)

        // 创建一个内置的“球体”形状；它的构造器有6个参数：名称、分段、直径、场景、可更新、侧向
        const sphere = Mesh.CreateSphere('sphere1', 16, 2, this.scene, false, Mesh.FRONTSIDE)

        // 创建一个内置的“地面”形状；它的构造函数采用6个参数：名称、宽度、高度、细分、场景、可更新
        // this.ground = Mesh.CreateGround('ground1', 6, 6, 2, this.scene, false)

        // 创建一个材质
        const myMaterial = new StandardMaterial('myMaterial', this.scene)
        // 漫反射颜色
        // myMaterial.diffuseColor = new Color3(1, 0, 1)
        // 镜面反射颜色
        // myMaterial.specularColor = new Color3(0.5, 0.6, 0.87)
        // 自发光颜色
        // myMaterial.emissiveColor = new Color3(1, 1, 1)
        // 环境颜色
        myMaterial.ambientColor = new Color3(0.23, 0.98, 0.53)
        // 给`球体`添加材质
        sphere.material = myMaterial

    }

    /**
     * 起飞
     */
    public launcher() {
        // 循环渲染
        this.engine.runRenderLoop(() => {
            this.scene.render()
        })
        

        // canvas/windows resize
        window.addEventListener('resize', () => {
            this.engine.resize()
        })
    }

}
