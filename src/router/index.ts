import { App } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import routes from './routes/index'

const router = createRouter({
    history: createWebHistory(import.meta.env.VITE_PUBLIC_BASE),
    routes
})

export function launchRouter(app: App) {
    app.use(router)
}

export default router

