
/**
 * 默认布局
 * @returns 默认布局
 */
export const LAYOUT_DEFAULT = () => import('@/layout/default/index.vue')

/**
 * 空布局
 * @returns 空布局
 */
export const LAYOUT_NULL = () => import('@/layout/null/index.vue')
