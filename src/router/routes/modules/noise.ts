import { RouteRecordRaw } from 'vue-router'
import { LAYOUT_NULL } from '@/router/constant'

export const routes: Array<RouteRecordRaw> = [
    {
        path: '/noise',
        name: 'Noise',
        component: LAYOUT_NULL,
        children: [
            {
                path: '',
                name: 'NoiseIndex',
                component: () => import('@/views/noise/index.vue'),
            },
            {
                path: 'perlin',
                name: 'PERLIN',
                component: () => import('@/views/noise/perlin.vue')
            }
        ]
    }
]

export default routes
