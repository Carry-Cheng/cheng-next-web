import { RouteRecordRaw } from 'vue-router'

export const routes: Array<RouteRecordRaw> = [
    {
        path: '/about',
        name: 'AboutUs',
        component: () => import('@/views/about-us/index.vue')
    }
]

export default routes
