import { RouteRecordRaw } from 'vue-router'

import { LAYOUT_NULL } from '@/router/constant'

export const routes: Array<RouteRecordRaw> = [
    {
        path: '/babylon',
        name: 'Babylon',
        component: LAYOUT_NULL,
        redirect: () => {
            return { name: 'BabylonIndex' }
        },
        children: [
            {
                path: '',
                name: 'BabylonIndex',
                component: () => import('@/views/babylon/index.vue')
            },
            {
                path: 'hello',
                name: 'BabylonHello',
                component: () => import('@/views/babylon/hello.vue')
            }
        ]
    }
]

export default routes
