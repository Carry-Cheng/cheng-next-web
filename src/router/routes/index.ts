import { RouteRecordRaw } from 'vue-router'

const modules = import.meta.globEager('./modules/*.ts')

const routes = new Array<RouteRecordRaw>()

// console.info(modules)
Object.keys(modules).forEach(key => {
    const mod = modules[key].default || {}
    const modList = Array.isArray(mod) ? [...mod] : [mod]
    routes.push(...modList)
})

export default routes

